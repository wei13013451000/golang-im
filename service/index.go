package service

import (
	"github.com/gin-gonic/gin"
	"text/template"
)

// GetIndex
// @Tags 首页模块
// @Summary 首页
// @Description 测试
// @Accept json
// @Produce json
// @Success 200 {string} ok
// @Router /index [get]
func GetIndex(c *gin.Context) {
	ind, err := template.ParseFiles("index.html", "views/chat/head.html")
	if err != nil {
		panic(err)
	}
	ind.Execute(c.Writer, "index")

	//ctx.JSON(200, gin.H{
	//	"msg": "ok",
	//})
}
func ToRegister(c *gin.Context) {
	ind, err := template.ParseFiles("views/user/register.html")
	if err != nil {
		panic(err)
	}
	ind.Execute(c.Writer, "register")
}
