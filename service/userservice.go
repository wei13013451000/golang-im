package service

import (
	"fmt"
	"ginchat/models"
	"ginchat/utils"
	"github.com/asaskevich/govalidator"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

// GetUserList
// @Summary 用户列表
// @Tags 用户模块
// @Description 获取所有用户列表
// @Accept json
// @Produce json
// @Success 200 {string} json{"code","message"}
// @Router /user/getUserList [get]
func GetUserList(c *gin.Context) {

	data := models.GetUserList()

	c.JSON(200, gin.H{
		"code":    200,
		"message": "获取成功!",
		"data":    data,
	})
}

// CreateUser
// @Summary 新增用户
// @Tags 用户模块
// @Description 新增用户测试
// @Accept json
// @Produce json
// @Param   name     formData    string     true        "用户名"
// @Param   password     formData    int     true        "密码"
// @Param   repassword      formData    int     true        "重复密码"
// @Success 200 {string}  json{"code","message"}
// @Router /user/createUser [get]
func CreateUser(c *gin.Context) {
	user := &models.UserBasic{}
	user.Name = c.Request.FormValue("name")
	password := c.Request.FormValue("password")
	repassword := c.Request.FormValue("repassword")

	//产生随机数存入数据库,用作密码加密
	salt := fmt.Sprintf("%06d", rand.Int31())

	if user.Name == "" || password == "" || repassword == "" {
		c.JSON(200, gin.H{
			"code":    -1, //  0成功   -1失败
			"message": "用户名或密码不能为空！",
			"data":    user,
		})
		return
	}
	if password != repassword {
		c.JSON(200, gin.H{
			"code":    -1, //  0成功   -1失败
			"message": "两次密码不一致！",
			"data":    user,
		})
		return
	}

	//密码加盐+加密
	makePassword := utils.MakePassword(password, salt)
	user.PassWord = makePassword
	user.Salt = salt

	data := models.FindUserByName(user.Name)
	if data.Name != "" {
		c.JSON(200, gin.H{
			"code":    -1, //  0成功   -1失败
			"message": "用户名已注册！",
			"data":    user,
		})
		return
	}

	fmt.Println(user.PassWord)

	models.CreateUser(user)
	c.JSON(200, gin.H{
		"code":    0, //  0成功   -1失败
		"message": "新增用户成功！",
		"data":    user,
	})
}

// DeleteUser
// @Summary 删除用户
// @Tags 用户模块
// @Description 删除用户
// @Accept json
// @Produce json
// @Param ID query integer true "用户ID"
// @Success 200 {string} json{"code","message"}
// @Router /user/deleteUser [get]
func DeleteUser(c *gin.Context) {

	id := c.Query("ID") //无法将 'c.Query("ID")' (类型 string) 用作类型 uint

	err := models.DeleteUser(id)
	if err != nil {
		c.JSON(-1, gin.H{
			"code":    -1,
			"message": err,
		})
	}

	c.JSON(200, gin.H{
		"code":    0,
		"message": "删除用户成功!",
	})
}

// UpdateUser
// @Summary 修改用户
// @Tags 用户模块
// @param id formData string false "id"
// @param name formData string false "name"
// @param password formData string false "password"
// @param phone formData string false "phone"
// @param email formData string false "email"
// @Success 200 {string} json{"code","message"}
// @Router /user/updateUser [post]
func UpdateUser(c *gin.Context) {
	user := models.UserBasic{}
	id, _ := strconv.Atoi(c.PostForm("id"))
	user.ID = uint(id)
	user.Name = c.PostForm("name")
	user.PassWord = c.PostForm("password")
	user.Phone = c.PostForm("phone")
	user.Avatar = c.PostForm("icon")
	user.Email = c.PostForm("email")
	fmt.Println("update :", user)
	_, err := govalidator.ValidateStruct(user)
	if err != nil {
		fmt.Println(err)
		c.JSON(200, gin.H{
			"code":    -1, //  0成功   -1失败
			"message": "修改参数不匹配！",
			"data":    user,
		})
	} else {
		models.UpdateUser(&user)
		c.JSON(200, gin.H{
			"code":    0, //  0成功   -1失败
			"message": "修改用户成功！",
			"data":    user,
		})
	}

}

// Login
// @Summary 登录
// @Tags 用户模块
// @param name formData string false "name"
// @param password formData string false "password"
// @Success 200 {string} json{"code","message"}
// @Router /user/login [post]
func Login(c *gin.Context) {
	var user models.UserBasic
	name := c.PostForm("name")
	pwd := c.PostForm("password")

	//查询是否存在该用户
	user = models.FindUserByName(name)
	if user.Name == "" {
		c.JSON(-1, gin.H{
			"code":    -1,
			"message": "用户名错误",
		})
		return
	}
	//判断密码
	if utils.ValidPassword(pwd, user.Salt, user.PassWord) == false {
		c.JSON(-1, gin.H{
			"code":    -1,
			"message": "密码错误",
		})
		return
	}

	c.JSON(200, gin.H{
		"code":    0, //  0成功   -1失败
		"message": "登录成功！",
		"data":    user,
	})

}

// 防止跨域站点伪造请求
var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func SendMsg(c *gin.Context) {
	// 升级HTTP连接到WebSocket
	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		fmt.Println("ws Upgrade err=", err)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	defer func(ws *websocket.Conn) {
		err := ws.Close()
		if err != nil {
			fmt.Println("ws Close err=", err)
		}
	}(ws)

	MsgHandler(c, ws)

}
func MsgHandler(c *gin.Context, ws *websocket.Conn) {
	for {

		msg, err := utils.Subscribe(c, utils.PublishKey)
		if err != nil {
			fmt.Println(" MsgHandler 发送失败", err)
		}

		tm := time.Now().Format("2006-01-02 15:04:05")
		m := fmt.Sprintf("[ws][%s]:%s", tm, msg)
		err = ws.WriteMessage(1, []byte(m))
		if err != nil {
			log.Fatalln(err)
		}
	}
}
func SendUserMsg(c *gin.Context) {
	models.Chat(c.Writer, c.Request)
}

//func RedisMsg(c *gin.Context) {
//	userIdA, _ := strconv.Atoi(c.PostForm("userIdA"))
//	userIdB, _ := strconv.Atoi(c.PostForm("userIdB"))
//	start, _ := strconv.Atoi(c.PostForm("start"))
//	end, _ := strconv.Atoi(c.PostForm("end"))
//	isRev, _ := strconv.ParseBool(c.PostForm("isRev"))
//	res := models.RedisMsg(int64(userIdA), int64(userIdB), int64(start), int64(end), isRev)
//	utils.RespOKList(c.Writer, "ok", res)
//}
//

//func SearchFriends(c *gin.Context) {
//	id, _ := strconv.Atoi(c.Request.FormValue("userId"))
//	users := models.SearchFriend(uint(id))
//	// c.JSON(200, gin.H{
//	// 	"code":    0, //  0成功   -1失败
//	// 	"message": "查询好友列表成功！",
//	// 	"data":    users,
//	// })
//	utils.RespOKList(c.Writer, users, len(users))
//}
//
//func AddFriend(c *gin.Context) {
//	userId, _ := strconv.Atoi(c.Request.FormValue("userId"))
//	targetName := c.Request.FormValue("targetName")
//	//targetId, _ := strconv.Atoi(c.Request.FormValue("targetId"))
//	code, msg := models.AddFriend(uint(userId), targetName)
//	if code == 0 {
//		utils.RespOK(c.Writer, code, msg)
//	} else {
//		utils.RespFail(c.Writer, msg)
//	}
//}
//
////新建群
//func CreateCommunity(c *gin.Context) {
//	ownerId, _ := strconv.Atoi(c.Request.FormValue("ownerId"))
//	name := c.Request.FormValue("name")
//	icon := c.Request.FormValue("icon")
//	desc := c.Request.FormValue("desc")
//	community := models.Community{}
//	community.OwnerId = uint(ownerId)
//	community.Name = name
//	community.Img = icon
//	community.Desc = desc
//	code, msg := models.CreateCommunity(community)
//	if code == 0 {
//		utils.RespOK(c.Writer, code, msg)
//	} else {
//		utils.RespFail(c.Writer, msg)
//	}
//}
//
////加载群列表
//func LoadCommunity(c *gin.Context) {
//	ownerId, _ := strconv.Atoi(c.Request.FormValue("ownerId"))
//	//	name := c.Request.FormValue("name")
//	data, msg := models.LoadCommunity(uint(ownerId))
//	if len(data) != 0 {
//		utils.RespList(c.Writer, 0, data, msg)
//	} else {
//		utils.RespFail(c.Writer, msg)
//	}
//}
//
////加入群 userId uint, comId uint
//func JoinGroups(c *gin.Context) {
//	userId, _ := strconv.Atoi(c.Request.FormValue("userId"))
//	comId := c.Request.FormValue("comId")
//
//	//	name := c.Request.FormValue("name")
//	data, msg := models.JoinGroup(uint(userId), comId)
//	if data == 0 {
//		utils.RespOK(c.Writer, data, msg)
//	} else {
//		utils.RespFail(c.Writer, msg)
//	}
//}
//
//func FindByID(c *gin.Context) {
//	userId, _ := strconv.Atoi(c.Request.FormValue("userId"))
//
//	//	name := c.Request.FormValue("name")
//	data := models.FindByID(uint(userId))
//	utils.RespOK(c.Writer, data, "ok")
//}
