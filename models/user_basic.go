package models

import (
	"database/sql"
	"ginchat/utils"
	"gorm.io/gorm"
)

type UserBasic struct {
	gorm.Model
	Name          string
	PassWord      string
	Phone         string `valid:"matches(^1[3-9]{1}\\d{9}$)"`
	Email         string `valid:"email"`
	Avatar        string //头像
	Identity      string //身份证
	ClientIp      string
	ClientPort    string
	Salt          string       //盐
	LoginTime     sql.NullTime // 使用 sql.NullTime 来表示可能为 NULL 的时间
	HeartBeatTime sql.NullTime
	LoginOutTime  sql.NullTime `gorm:"column:login_out_time" json:"login_out_time"`
	IsLogOut      bool
	DeviceInfo    string
}

func (table *UserBasic) TableName() string {
	return "user_basic"
}

func FindUserByNameAndPassword(name, password string) (UserBasic, error) {
	var user UserBasic
	tx := utils.DB.Where("name = ? and pass_word = ?", name, password).First(&user)
	if tx.Error != nil {
		return UserBasic{}, tx.Error
	}

	return user, nil

}

func GetUserList() []*UserBasic {
	data := make([]*UserBasic, 10)
	utils.DB.Find(&data)
	return data
}

func CreateUser(user *UserBasic) *gorm.DB {
	return utils.DB.Create(&user)
}

func DeleteUser(id string) *gorm.DB {
	return utils.DB.Delete(&UserBasic{}, "id=?", id)

}

func UpdateUser(user *UserBasic) *gorm.DB {
	return utils.DB.Model(&user).Updates(UserBasic{Name: user.Name, PassWord: user.PassWord, Phone: user.Phone, Email: user.Email, Avatar: user.Avatar})
}

func FindUserByName(name string) UserBasic {
	user := UserBasic{}
	utils.DB.Where("name = ?", name).First(&user)
	return user
}
func FindUserByPhone(phone string) UserBasic {
	user := UserBasic{}
	utils.DB.Where("Phone = ?", phone).First(&user)
	return user
}
func FindUserByEmail(email string) UserBasic {
	user := UserBasic{}
	utils.DB.Where("email = ?", email).First(&user)
	return user
}
