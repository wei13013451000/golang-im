package models

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"gopkg.in/fatih/set.v0"
	"gorm.io/gorm"
	"net"
	"net/http"
	"strconv"
	"sync"
)

// Message 消息表
type Message struct {
	gorm.Model
	FromId   int64  //发送者
	TargetId int64  //接受者
	Type     int    //消息类型 群聊 私聊 广播
	Media    int    //消息类型 文字 图片 声音
	Content  string //消息内容
	Pic      string
	Url      string
	Desc     string
	Amount   int //其他数字统计
}

func (table *Message) TableName() string {
	return "message"
}

type Node struct {
	Conn      *websocket.Conn //连接
	DataQueue chan []byte     //消息队列
	GroupSets set.Interface   //操作方法接口
}

// 映射关系
var clientMap map[int64]*Node = make(map[int64]*Node)

// 读写锁
var rwLocker sync.RWMutex

func Chat(writer http.ResponseWriter, request *http.Request) {
	fmt.Println(">>>>调用 Chat")

	//1.获取参数,校验token等合法性
	//token := query.Get("token")

	query := request.URL.Query()
	id := query.Get("userId")
	userId, _ := strconv.ParseInt(id, 10, 64)
	//targetId := query.Get("targetId")
	//msgType := query.Get("type")
	//content := query.Get("content")
	isValidate := true //校验token
	conn, err := (&websocket.Upgrader{
		//校验token
		CheckOrigin: func(r *http.Request) bool {
			return isValidate
		},
	}).Upgrade(writer, request, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	//2.获取连接 conn
	node := &Node{
		Conn:      conn,
		DataQueue: make(chan []byte, 50),
		GroupSets: set.New(set.ThreadSafe), //创建一个可以在多线程使用的安全集合,内部会被保护
	}

	//3.用户关系
	//4.userId跟node绑定,加锁
	rwLocker.Lock()
	clientMap[userId] = node
	rwLocker.Unlock()

	//5.完成发送逻辑
	go sendProc(node)
	//6.完成接收逻辑
	go recvProc(node)
	sendMsg(userId, []byte("欢迎进入聊天系统!"))

}

func sendProc(node *Node) {
	fmt.Println(">>>>调用 sendProc")

	for {
		select {
		case dataQueue := <-node.DataQueue:
			err := node.Conn.WriteMessage(websocket.TextMessage, dataQueue)
			if err != nil {
				fmt.Println("node.Conn.WriteMessage ", err)
				return
			}
		}
	}
}

func recvProc(node *Node) {
	fmt.Println(">>>>调用 recvProc")

	for {
		_, data, err := node.Conn.ReadMessage()
		if err != nil {
			fmt.Println("node.Conn.ReadMessage err", err)
			return
		}
		broadMsg(data)

		//fmt.Println("recvProc node.Conn.ReadMessage ", data)
	}
}

var udpsendChan chan []byte = make(chan []byte, 1)

func broadMsg(data []byte) {
	fmt.Println(">>>>调用 broadMsg")

	udpsendChan <- data
}

func init() {
	fmt.Println(">>>>调用 init")

	go udpsendProc()
	go updRecvProc()
}

// 完成udp数据发送协程
func udpsendProc() {
	fmt.Println(">>>>调用 udpsendProc")

	dialUDP, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   net.IPv4(192, 168, 0, 255),
		Port: 3000,
	})

	defer func(con *net.UDPConn) {
		err := con.Close()
		if err != nil {
			fmt.Println("UDPConn close err", err)
		}
	}(dialUDP)
	if err != nil {
		fmt.Println(err)
	}
	for {
		select {
		case data := <-udpsendChan:
			dialUDPWriteData, err := dialUDP.Write(data)
			if err != nil {
				fmt.Println("dialUDP.Write err", err)
				return
			}
			fmt.Println("<<<dialUDP WriteData=", dialUDPWriteData)
		}
	}

}

// 完成udp数据接收协程
func updRecvProc() {
	fmt.Println(">>>>调用 updRecvProc")

	listenUDP, err := net.ListenUDP("udp", &net.UDPAddr{
		IP:   net.IPv4zero,
		Port: 3000,
	})
	if err != nil {
		fmt.Println("ListenUDP err", err)
	}
	defer func(listenUDP *net.UDPConn) {
		err := listenUDP.Close()
		if err != nil {
			fmt.Println("listenUDP.Close err", err)
		}
	}(listenUDP)

	for {
		var buf [512]byte
		n, err := listenUDP.Read(buf[0:])
		if err != nil {
			fmt.Println("ListenUDP Read err", err)
			return
		}
		fmt.Println("<<<ListenUDP Read data=", string(buf[0:n]))
		dispatch(buf[0:n])

	}

}

// 后端调度逻辑处理
func dispatch(data []byte) {
	fmt.Println(">>>>调用 dispatch 后端调度逻辑处理")

	msg := Message{}
	err := json.Unmarshal(data, &msg)
	if err != nil {
		fmt.Println(err)
		return
	}
	switch msg.Type {
	case 1:
		sendMsg(msg.TargetId, data) //私信
		//case 2:
		//	sendGrounMsg() //群发
		//case 3: sendAllMsg() //广播
		//case 4:
	}

}

func sendMsg(userId int64, msg []byte) {
	fmt.Printf(">>>>调用 sendMsg 把消息写进userId =%d的DataQueue:", userId)
	rwLocker.RLock()
	node, ok := clientMap[userId]
	defer rwLocker.RUnlock()
	if ok {
		node.DataQueue <- msg
	}
}
