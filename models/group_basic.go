package models

import "gorm.io/gorm"

// GroupBasic 群信息
type GroupBasic struct {
	gorm.Model
	Name    string //
	OwnerId uint   //谁的关系信息
	Icon    string
	Type    int //对应的类型 0 1 3
	Desc    string
}

func (table *GroupBasic) TableName() string {
	return "Group_basic"
}
