package utils

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

func InitConfig() {
	viper.SetConfigName("app")
	viper.AddConfigPath("config")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("viper.ReadInConfig err=", err)
	}
	fmt.Println("::viper read config success")

}
func InitRedis() {
	// 创建 Redis 客户端
	client := redis.NewClient(&redis.Options{
		Addr:         viper.GetString("redis.addr"),
		Password:     viper.GetString("redis.password"),
		DB:           viper.GetInt("redis.DB"),
		PoolSize:     viper.GetInt("redis.poolSize"),
		MinIdleConns: viper.GetInt("redis.minIdleConn"),
	})

	// 检查 Redis 连接
	ctx := context.Background()
	if err := client.Ping(ctx).Err(); err != nil {
		fmt.Println("::failed to connect redis", err)
		return
	}
	fmt.Println("::redis connect success")

	// 设置键值对
	setCmd := client.Set(ctx, "testKey", "testValue", 0) // 使用0作为过期时间，表示永不过期
	if err := setCmd.Err(); err != nil {
		fmt.Println("::failed to set value to redis", err)
		return
	}

	// 检查键值对是否设置成功
	getCmd := client.Get(ctx, "testKey")
	if getCmd.Val() != "testValue" {
		fmt.Println("::failed to retrieve value from redis")
		return
	}

	// 如果所有操作都成功，则将连接保存在全局变量中
	Red = client
}
func InitMySql() {
	//gorm 日志
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{
			SlowThreshold: time.Second, //阈值
			LogLevel:      logger.Info, //打印级别
			Colorful:      true,        //彩色
		})
	var err error
	DB, err = gorm.Open(mysql.Open(viper.GetString("mysql.dns")), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		panic("::failed to connect database")
	}
	fmt.Println("::connect database success")
}

var (
	DB  *gorm.DB
	Red *redis.Client
)

const (
	PublishKey = "websocket"
)

// Publish 发布消息到Redis
func Publish(ctx context.Context, channel string, msg string) error {
	var err error
	fmt.Println("::redis Publish 。。。。", msg)
	err = Red.Publish(ctx, channel, msg).Err()
	if err != nil {
		fmt.Println(err)
	}
	return err
}

// Subscribe 订阅Redis消息
func Subscribe(ctx context.Context, channel string) (string, error) {
	sub := Red.Subscribe(ctx, channel)
	fmt.Println("::redis Subscribe1 。。。。", sub)
	msg, err := sub.ReceiveMessage(ctx)
	fmt.Println("::redis Subscribe1 。。。。", msg)

	if err != nil {
		fmt.Println(err)
		return "", err
	}
	fmt.Println("::redis Subscribe2 。。。。", msg.Payload)
	return msg.Payload, err
}
