# golang-IM

#### 介绍
golang IM 即时通讯系统

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

调试工具

https://www.json.cn/network/websocket/



# 一.gin项目初始化及Gorm引入

## 1.设置编辑器

`GOPROXY=https://goproxy.cn,direct`

![image-20240715165847956](README/image-20240715165847956.png)

```shell
#初始化
go mod init ginchat

go mod tidy

#引入gorm 包
go get gorm.io/gorm

```

## 2.创建实体类

```go
package models

import "gorm.io/gorm"

type UserBasic struct {
	gorm.Model
	Name          string
	PassWord      string
	Phone         string
	Email         string
	Identity      string
	ClientIp      string
	ClientPort    string
	LoginTime     uint64
	HeartBeatTime uint64
	LogOutTime    uint64
	IsLogOut      bool
	DeviceInfo    string
}

func (table *UserBasic) TableName() string {
	return "user_basic"
}

```



## 3.测试

```go

package main

import (
	"fmt"
	"ginchat/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	db, err := gorm.Open(mysql.Open("root:123456@tcp(127.0.0.1:3306)/gin_chat?charset=utf8mb4&parseTime=True&loc=Local"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	// 迁移 schema
	db.AutoMigrate(&models.UserBasic{})

	// Create
	user := &models.UserBasic{}
	user.Name = "小明"
	db.Create(user)

	// Read
	fmt.Println(db.First(user, 1)) // 根据整型主键查找
	//db.First(&product, "code = ?", "D42") // 查找 code 字段值为 D42 的记录

	// Update - 将 product 的 price 更新为 200
	db.Model(user).Update("PassWord", "123456")
	// Update - 更新多个字段
	//db.Model(&product).Updates(Product{Price: 200, Code: "F42"}) // 仅更新非零值字段
	//db.Model(&product).Updates(map[string]interface{}{"Price": 200, "Code": "F42"})

	// Delete - 删除 product
	//db.Delete(&product, 1)
}

```

## 4.结果

- 生成数据表字段,并成功修改数据

  ![image-20240715173115814](README/image-20240715173115814.png)

# 二.gin引入以及项目目录建立

```shell
go get github.com/gin-gonic/gin

# 示例代码
package main

import (
	"ginchat/router"
	"github.com/gin-gonic/gin"
	"log"
)

func main() {
	engine := gin.Default()
	engine.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	
	err := engine.Run()
	if err != nil {
		log.Fatal(err)
		return
	}

	r := router.Router()
	r.Run(":3000")
}

```

## 1.拆分示例代码到各个包(router/Service/main)

![image-20240715212109098](README/image-20240715212109098.png)

![image-20240715212123382](README/image-20240715212123382.png)

![image-20240715212138255](README/image-20240715212138255.png)

## 2.运行结果

![image-20240715212222777](README/image-20240715212222777.png)

# 三.gorm补充

![image-20240715215225784](README/image-20240715215225784.png)

![image-20240715215400307](README/image-20240715215400307.png)

## 1.运行结构

![image-20240715215446269](README/image-20240715215446269.png)

# 四.前后端交互

## 1.初始化读取配置文件,数据库

![image-20240715234342821](README/image-20240715234342821.png)

![image-20240715233801786](README/image-20240715233801786.png)

![image-20240715233731430](README/image-20240715233731430.png)

![image-20240715233915792](README/image-20240715233915792.png)

![image-20240715233959024](README/image-20240715233959024.png)

## 2.结果

![image-20240715234021785](README/image-20240715234021785.png)

# 五. 前后端分离swagger引入

## 1.安装

```shell
go install github.com/swaggo/swag/cmd/swag@latest

swag init

go get -u github.com/swaggo/gin-swagger
go get -u github.com/swaggo/files


```

## 2.配置

![image-20240716004641833](README/image-20240716004641833.png)

![image-20240716004723286](README/image-20240716004723286.png)

配置完后需要执行`swag init`

## 3. 结果

![image-20240716004807859](README/image-20240716004807859.png)

# 六.gorm mysql日志配置

## 1.配置

![image-20240716005831860](README/image-20240716005831860.png)

## 2.结果

![image-20240716005925402](README/image-20240716005925402.png)

# 七.修改用户

## 1.代码

![image-20240716130027624](README/image-20240716130027624.png)

![image-20240716130122644](README/image-20240716130122644.png)

![image-20240716130157587](README/image-20240716130157587.png)

## 2.结果

![image-20240716130240948](README/image-20240716130240948.png)

![image-20240716130255141](README/image-20240716130255141.png)

# 八.修改邮箱,电话校验

```shell

go get github.com/asaskevich/govalidator
```

![image-20240716131821364](README/image-20240716131821364.png)

![image-20240716131843686](README/image-20240716131843686.png)

# 九.重复注册校验

![image-20240716135122173](README/image-20240716135122173.png)

![image-20240716135133572](README/image-20240716135133572.png)

# 十.MD5密码注册密码加密

```go

package utils
import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"
)

//小写
func Md5Encode(data string) string {
	h := md5.New()
	h.Write([]byte(data))
	tempStr := h.Sum(nil)
	return hex.EncodeToString(tempStr)
}

//大写
func MD5Encode(data string) string {
	return strings.ToUpper(Md5Encode(data))
}

//加密
func MakePassword(plainpwd, salt string) string {
	return Md5Encode(plainpwd + salt)
}

//解密
func ValidPassword(plainpwd, salt string, password string) bool {
	md := Md5Encode(plainpwd + salt)
	fmt.Println(md + "				" + password)
	return md == password
}
```

![image-20240716142008646](README/image-20240716142008646.png)

![image-20240716142117760](README/image-20240716142117760.png)

![image-20240716142133220](README/image-20240716142133220.png)

# 十一.登录

```go

// Login
// @Summary 登录
// @Tags 用户模块
// @param name formData string false "name"
// @param password formData string false "password"
// @Success 200 {string} json{"code","message"}
// @Router /user/login [post]
func Login(c *gin.Context) {
	var user models.UserBasic
	name := c.PostForm("name")
	pwd := c.PostForm("password")

	//查询是否存在该用户
	user = models.FindUserByName(name)
	if user.Name == "" {
		c.JSON(-1, gin.H{
			"message": "用户名错误",
		})
		return
	}
	//判断密码
	if utils.ValidPassword(pwd, user.Salt, user.PassWord) == false {
		c.JSON(-1, gin.H{
			"message": "密码错误",
		})
		return
	}

	c.JSON(200, gin.H{
		"code":    0, //  0成功   -1失败
		"message": "登录成功！",
		"data":    user,
	})

}
```

# 十二.token加入

# 十三.redis+websocket

```shell
go get github.com/go-redis/redis/v8
github.com/gorilla/websocket


```

```yml
mysql:
  dns: root:123456@tcp(127.0.0.1:3306)/gin_chat?charset=utf8mb4&parseTime=True&loc=Local
redis:
  addr: "127.0.0.1:6379"
  password: ""
  DB: 0
  poolSize: 30
  minIdleConn: 30
oss:
  Endpoint: "oss-cn-hangzhou.aliyuncs.com"
  AccessKeyId: "LTAI5tNCXPJwS3MstKoKgixh"
  AccessKeySecret: "YhHE8OyCMsqfjwOnxQ1oO7paYlDjVHX"
  Bucket : "ginchat"

timeout:
  DelayHeartbeat: 3   #延迟心跳时间  单位秒
  HeartbeatHz: 30   #每隔多少秒心跳时间
  HeartbeatMaxTime: 30000  #最大心跳时间  ，超过此就下线
  RedisOnlineTime: 4  #缓存的在线用户时长   单位H

port:
  server: ":8082"
  udp: 3001
```

```go
func InitRedis() {
	client := redis.NewClient(&redis.Options{
		Addr:         viper.GetString("redis.addr"),
		Password:     viper.GetString("redis.password"),
		DB:           viper.GetInt("redis.DB"),
		PoolSize:     viper.GetInt("redis.poolSize"),
		MinIdleConns: viper.GetInt("redis.minIdleConn"),
	})
	if err := client.Ping(context.Background()).Err(); err != nil {
		fmt.Println("::failed to connect redis", err)
	}
	fmt.Println("::redis connect success")
	Red = client
}
```

# 十四.发送接受消息实现骨架

```shell
go get gopkg.in/fatih/set.v0 //用于提供集合（Set）数据结构
```

```go
package models

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"gopkg.in/fatih/set.v0"
	"gorm.io/gorm"
	"net"
	"net/http"
	"strconv"
	"sync"
)

// Message 消息表
type Message struct {
	gorm.Model
	FromId   int64  //发送者
	TargetId int64  //接受者
	Type     int    //消息类型 群聊 私聊 广播
	Media    int    //消息类型 文字 图片 声音
	Content  string //消息内容
	Pic      string
	Url      string
	Desc     string
	Amount   int //其他数字统计
}

func (table *Message) TableName() string {
	return "message"
}

type Node struct {
	Conn      *websocket.Conn //连接
	DataQueue chan []byte     //消息队列
	GroupSets set.Interface   //操作方法接口
}

// 映射关系
var clientMap map[int64]*Node = make(map[int64]*Node)

// 读写锁
var rwLocker sync.RWMutex

func Chat(writer http.ResponseWriter, request *http.Request) {
	fmt.Println(">>>>调用 Chat")

	//1.获取参数,校验token等合法性
	//token := query.Get("token")

	query := request.URL.Query()
	id := query.Get("userId")
	userId, _ := strconv.ParseInt(id, 10, 64)
	//targetId := query.Get("targetId")
	//msgType := query.Get("type")
	//content := query.Get("content")
	isValidate := true //校验token
	conn, err := (&websocket.Upgrader{
		//校验token
		CheckOrigin: func(r *http.Request) bool {
			return isValidate
		},
	}).Upgrade(writer, request, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	//2.获取连接 conn
	node := &Node{
		Conn:      conn,
		DataQueue: make(chan []byte, 50),
		GroupSets: set.New(set.ThreadSafe), //创建一个可以在多线程使用的安全集合,内部会被保护
	}

	//3.用户关系
	//4.userId跟node绑定,加锁
	rwLocker.Lock()
	clientMap[userId] = node
	rwLocker.Unlock()

	//5.完成发送逻辑
	go sendProc(node)
	//6.完成接收逻辑
	go recvProc(node)
	sendMsg(userId, []byte("欢迎进入聊天系统!"))

}

func sendProc(node *Node) {
	fmt.Println(">>>>调用 sendProc")

	for {
		select {
		case dataQueue := <-node.DataQueue:
			err := node.Conn.WriteMessage(websocket.TextMessage, dataQueue)
			if err != nil {
				fmt.Println("node.Conn.WriteMessage ", err)
				return
			}
		}
	}
}

func recvProc(node *Node) {
	fmt.Println(">>>>调用 recvProc")

	for {
		_, data, err := node.Conn.ReadMessage()
		if err != nil {
			fmt.Println("node.Conn.ReadMessage err", err)
			return
		}
		broadMsg(data)

		//fmt.Println("recvProc node.Conn.ReadMessage ", data)
	}
}

var udpsendChan chan []byte = make(chan []byte, 1)

func broadMsg(data []byte) {
	fmt.Println(">>>>调用 broadMsg")

	udpsendChan <- data
}

func init() {
	fmt.Println(">>>>调用 init")

	go udpsendProc()
	go updRecvProc()
}

// 完成udp数据发送协程
func udpsendProc() {
	fmt.Println(">>>>调用 udpsendProc")

	dialUDP, err := net.DialUDP("udp", nil, &net.UDPAddr{
		IP:   net.IPv4(192, 168, 0, 255),
		Port: 3000,
	})

	defer func(con *net.UDPConn) {
		err := con.Close()
		if err != nil {
			fmt.Println("UDPConn close err", err)
		}
	}(dialUDP)
	if err != nil {
		fmt.Println(err)
	}
	for {
		select {
		case data := <-udpsendChan:
			dialUDPWriteData, err := dialUDP.Write(data)
			if err != nil {
				fmt.Println("dialUDP.Write err", err)
				return
			}
			fmt.Println("<<<dialUDP WriteData=", dialUDPWriteData)
		}
	}

}

// 完成udp数据接收协程
func updRecvProc() {
	fmt.Println(">>>>调用 updRecvProc")

	listenUDP, err := net.ListenUDP("udp", &net.UDPAddr{
		IP:   net.IPv4zero,
		Port: 3000,
	})
	if err != nil {
		fmt.Println("ListenUDP err", err)
	}
	defer func(listenUDP *net.UDPConn) {
		err := listenUDP.Close()
		if err != nil {
			fmt.Println("listenUDP.Close err", err)
		}
	}(listenUDP)

	for {
		var buf [512]byte
		n, err := listenUDP.Read(buf[0:])
		if err != nil {
			fmt.Println("ListenUDP Read err", err)
			return
		}
		fmt.Println("<<<ListenUDP Read data=", string(buf[0:n]))
		dispatch(buf[0:n])

	}

}

// 后端调度逻辑处理
func dispatch(data []byte) {
	fmt.Println(">>>>调用 dispatch 后端调度逻辑处理")

	msg := Message{}
	err := json.Unmarshal(data, &msg)
	if err != nil {
		fmt.Println(err)
		return
	}
	switch msg.Type {
	case 1:
		sendMsg(msg.TargetId, data) //私信
		//case 2:
		//	sendGrounMsg() //群发
		//case 3: sendAllMsg() //广播
		//case 4:
	}

}

func sendMsg(userId int64, msg []byte) {
	fmt.Printf(">>>>调用 sendMsg 把消息写进userId =%d的DataQueue:", userId)
	rwLocker.RLock()
	node, ok := clientMap[userId]
	defer rwLocker.RUnlock()
	if ok {
		node.DataQueue <- msg
	}
}

```

![image-20240716233827012](README/image-20240716233827012.png)

![image-20240716233850804](README/image-20240716233850804.png)

![image-20240716233915492](README/image-20240716233915492.png)

# 十五.前端调试+登录,注册

## 1.引入静态资源

![image-20240717010120720](README/image-20240717010120720.png)

## 2.配置代码

![image-20240717010050101](README/image-20240717010050101.png)

![image-20240717010147661](README/image-20240717010147661.png)
